﻿using UnityEngine;
using System.Collections;

public class moveCamAround : MonoBehaviour 
{
	private Vector3 startPos;
	public Vector3 size;

	void Start ( ) 
	{
		startPos = this.transform.position;
	}
	
	void Update ( ) 
	{	

		this.transform.position = startPos + new Vector3(Mathf.Sin(Time.time) * size.x * GetSpectrumData.loudness/100, Mathf.Sin (Time.time) * size.y, Mathf.Cos(Time.time) * size.z);;
		Debug.DrawLine(this.transform.position, this.transform.position + new Vector3(0.1f,0.1f,0.1f), Color.red, 20.0f);

	}
}
