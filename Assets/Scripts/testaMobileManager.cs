﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


public class testaMobileManager : MonoBehaviour 
{
	ArtNet.Engine 						ArtEngine; 
	public GUISkin						myCustomGUISkin;						// declare public in order to put inside the myCustomGUISkin ( asset->create->GuiSkin )
	private int							dmxCh;		 
	private float[] 					hSliderValue; 							// values of each sliders
	private byte[]						bb;										// storing bytes to send 	
	private byte[] 						DMXData 		= new byte[512];
	private string[] 					type; 	
	
	void Start () 
	{
		dmxCh		 		=   13;											// number of dmx moving head channels
		hSliderValue 		= 	new float[dmxCh];	
		type 				=   new string[13] { "dd", "Pan" , "fineP" , "Tilt" , "fineT" , "Speed" , "R", "G", "B", "","" , "", "" };
		initDmx();
	}
	
	void Update () 
	{
		movingHead ();
		ArtEngine.SendDMX (0, DMXData, DMXData.Length); 
	}

	void initDmx () 
	{
		ArtEngine = new ArtNet.Engine("Open DMX Ethernet", "255.255.255.255");
		ArtEngine.Start ();
	}

	void OnGUI () 
	{
		// use myCustomGUISkin placed into asset folder
		GUI.skin = myCustomGUISkin;
		GUI.Box(new Rect(10,10,300,320), " DMX Moving Head Light Manager ");
		float gap = 20.0f;
		for(int i = 1; i< dmxCh; i++)
		{
			hSliderValue[i] = GUI.HorizontalSlider (new Rect (60, 50+(i*gap), 200, 10), hSliderValue[i], 0.0f, 255.0f);
			byte bb = Convert.ToByte((int)hSliderValue[i]);
			GUI.Label(new Rect(260, 45+(i*gap), 100, 30), bb.ToString());
			GUI.Label(new Rect(15,  45+(i*gap), 100,30), type[i].ToString());
			DMXData[i] = Convert.ToByte((int)hSliderValue[i]);
		}
	}	

	void movingHead ()
	{
			// PAN
		this.transform.rotation = Quaternion.Euler(new Vector3(0, -hSliderValue[1], 0));
			// TILT
		foreach( Transform child in transform)
		{	
			child.transform.localRotation =  Quaternion.Euler(new Vector3(ReMap.Remap( -hSliderValue[3],0,255, 285, 100), 0, 0 ));
		}
			// R G B
		Material beamerMat = Resources.Load("Materials/beam-light", typeof(Material)) as Material;
		beamerMat.color = new Color( ReMap.Remap(hSliderValue[6],0,255,0,1), ReMap.Remap(hSliderValue[7],0,255,0,1), ReMap.Remap(hSliderValue[8],0,255,0,1) );
	}

}