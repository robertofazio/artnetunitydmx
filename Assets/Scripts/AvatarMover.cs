﻿using UnityEngine;
using System.Collections;

public class AvatarMover : MonoBehaviour 
{

	public Vector3 	rotMax;
	public float	rotSpeed;
	public Vector3	floatMax;
	public float	floatSpeed;



	protected Vector3 	startingRot;
	protected Vector3 	startingPos;
	protected float 	timeGap;

	// Use this for initialization
	void Start () 
	{
		startingRot = transform.localRotation.eulerAngles;
		startingPos = transform.localPosition;

		timeGap = Random.Range( 0f, 100f );

	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localRotation = Quaternion.Euler( startingRot + rotMax * Mathf.Sin( Time.time*rotSpeed + timeGap ) );
		transform.localPosition = startingPos + floatMax*Mathf.Sin( Time.time*floatSpeed + timeGap ) ;
	}
}
