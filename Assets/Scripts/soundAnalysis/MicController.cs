﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * code by stefano.cecere@krur.com
 * class to manage the audio in (mic) and soudn analysis (bands, loudness, pitch)
 *
 * credits, inspiration, snippets & info:  
 * http://answers.unity3d.com/questions/394158/real-time-microphone-line-input-fft-analysis.html
 * http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html
 * http://forum.unity3d.com/threads/119595-Using-device-microphone-to-interact-with-objects
 */

[RequireComponent(typeof(AudioSource))]
public class MicController : MonoBehaviour
{
    public GameObject debugDisplay;
    public string microphoneDevice = "Default";
    public int nWindow = 20;
    public float ledScaler = 10.0f;
    private float fMax;
    private int quantiLed = 19;
    public float sensitivity = 200.0f;
    public float loudness = 0.0f;
    public float frequency = 0.0f;
    public int samplerate = 11024;
    public float threshold = 5.0f;
    public float maxLoudness = 1000.0f;
    public float minFreq = 1.0f;
    public float maxFreq = 500.0f;

    public GameObject[] TonCursors;
//    public UISlider FreqSlider;

    //    private const int FREQUENCY = 48000;    // Wavelength, I think.
    private const int SAMPLECOUNT = 1024;   // Sample Count.
    private const float REFVALUE = 0.1f;    // RMS value for 0 dB.
    private const float THRESHOLD = 0.02f;  // Minimum amplitude to extract pitch (recieve anything)
    private const float ALPHA = 0.05f;      // The alpha for the low pass filter (I don't really understand this).

    public int recordedLength = 50;    // How many previous frames of sound are analyzed.
    public int requiedBlowTime = 4;    // How long a blow must last to be classified as a blow (and not a sigh for instance).
    public int clamp = 160;            // Used to clamp dB (I don't really understand this either).
    
    private float rmsValue;            // Volume in RMS
    private float dbValue;             // Volume in DB
    private float pitchValue;          // Pitch - Hz (is this frequency?)
    private int blowingTime;           // How long each blow has lasted
    
    private float lowPassResults;      // Low Pass Filter result
    private float peakPowerForChannel; //

    private float[] samplesData;           // Samples
    private float[] spectrumData;          // Spectrum
    private List<float> dbValues;      // Used to average recent volume.
    private List<float> pitchValues;   // Used to average recent pitch.

    void Start() {
        samplerate = AudioSettings.outputSampleRate;
        fMax = samplerate / 2;
        spectrumData = new float[SAMPLECOUNT];
        samplesData = new float[SAMPLECOUNT];

        dbValues = new List<float> ();
        pitchValues = new List<float> ();

        StartMicListener ();
    }

    void Update() {
        audio.GetOutputData (samplesData, 0);
        audio.GetSpectrumData (spectrumData, 0, FFTWindow.BlackmanHarris);

        loudness = GetAveragedVolume () * sensitivity;
        frequency = GetFundamentalFrequency ();
        
        if (loudness > threshold) {
            float scaledFreq = Mathf.Clamp01 ((frequency - minFreq) / (maxFreq / minFreq));
            float scaledLoudness = (loudness - threshold) / (maxLoudness / threshold);
            
            for (var i=0; i <= quantiLed; i++) {
                float scaler = BandVol (i * nWindow, i * nWindow + nWindow);
                TonCursors [i].transform.localScale = new Vector3 (1.0f + scaler * ledScaler, 1.0f, 0);
            }
            //GameManager.Instance.ShoutMessage (scaledLoudness, scaledFreq);
            
            // TonCursor.transform.localScale = new Vector3 (0.3f + scaledLoudness, 0.3f + scaledLoudness, 0);
        } else {
         //   GameManager.Instance.StopShoutMessage ();
            for (var i=0; i <= quantiLed; i++) {
                //TonCursors [i].transform.localScale = new Vector3 (1.0f, 1.0f, 0);
            }
        }

        AnalyzeSound ();
        DeriveBlow ();

        float myValue = Mathf.Clamp01 ((pitchValue - minFreq) / (maxFreq / minFreq));
		Debug.Log(myValue);

        // zoomo mirino
        //GameManager.Instance.GUImngr.amplifyMirino(rmsValue);

        //debugDisplay.guiText.text += "\n" + "samplerate: " + samplerate;
        //debugDisplay.guiText.text += "\n" + "RMS: " + loudness.ToString ("F2") + " Pitch: " + frequency.ToString ("F0") + " Hz";
        debugDisplay.guiText.text += "\n" + "RMS: " + rmsValue.ToString ("F2") + " (" + dbValue.ToString ("F1") + " dB)\n" + "Pitch: " + pitchValue.ToString ("F0") + " Hz";
    }

    private void StartMicListener() {

        //make sure it's okay to start the microphone
        if (audio && Microphone.devices != null && Microphone.devices.Length > 0) {
            //get index
            int index = 0;
            for (int i = 0; i < Microphone.devices.Length; i++) {
                if (Microphone.devices [i].Equals (microphoneDevice, System.StringComparison.CurrentCultureIgnoreCase)) {
                    index = i;
                    break;
                }
            }
            
            //audio.clip = Microphone.Start(Microphone.devices[index], true, 999, AudioSettings.outputSampleRate);
            audio.clip = Microphone.Start (Microphone.devices [index], true, 999, samplerate);
            //audio.loop = true; // Set the AudioClip to loop
            audio.mute = true;
            
            //HACK - Wait for microphone to be ready
            while (!(Microphone.GetPosition(Microphone.devices[index]) > 0)) {
            }
            audio.Play ();
        } else if (audio) {
            Debug.LogWarning ("No audio source was found, can't start microphone! You must attach thise script to the same Game Object as an Audio Source.");
        } else if (Microphone.devices == null || Microphone.devices.Length <= 0) {
            Debug.LogWarning ("No microphone devices were found, can't start microphone!");
        } else {
            Debug.LogWarning ("Unknown issue, can't start microphone!");
        }
    }

    float BandVol(float fLow, float fHigh) {
        
        fLow = Mathf.Clamp (fLow, 20, fMax); // limit low...
        fHigh = Mathf.Clamp (fHigh, fLow, fMax); // and high frequencies

        int n1 = Mathf.FloorToInt (fLow * SAMPLECOUNT / fMax);
        int n2 = Mathf.FloorToInt (fHigh * SAMPLECOUNT / fMax);
        float sum = 0.0f;
        // average the volumes of frequencies fLow to fHigh
        for (var i=n1; i<=n2; i++) {
            sum += spectrumData [i];
        }
        return sum / (n2 - n1 + 1);
    }

    float GetAveragedVolume() {

        float a = 0;

        foreach (float s in samplesData) {
            a += Mathf.Abs (s);
        }
        return a / 256;
    }
    
    float GetFundamentalFrequency() {
        float fundamentalFrequency = 0.0f;

        float s = 0.0f;
        int i = 0;
        for (int j = 1; j < SAMPLECOUNT; j++) {
            if (s < spectrumData [j]) {
                s = spectrumData [j];
                i = j;
            }
        }
        fundamentalFrequency = i * samplerate / SAMPLECOUNT;
        return fundamentalFrequency;
    }



    /// Credits to aldonaletto for the function, http://goo.gl/VGwKt
    /// Analyzes the sound, to get volume and pitch values.
    private void AnalyzeSound() {
               
        // Sums squared samples
        float sum = 0;
        for (int i = 0; i < SAMPLECOUNT; i++) {
            sum += Mathf.Pow (samplesData [i], 2);
        }
        
        // RMS is the square root of the average value of the samples.
        rmsValue = Mathf.Sqrt (sum / SAMPLECOUNT);
        dbValue = 20 * Mathf.Log10 (rmsValue / REFVALUE);
        
        // Clamp it to {clamp} min
        if (dbValue < -clamp) {
            dbValue = -clamp;
        }
        
        // Gets the sound spectrum.
        float maxV = 0;
        int maxN = 0;
        
        // Find the highest sample.
        for (int i = 0; i < SAMPLECOUNT; i++) {
            if (spectrumData [i] > maxV && spectrumData [i] > THRESHOLD) {
                maxV = spectrumData [i];
                maxN = i; // maxN is the index of max
            }
        }
        
        // Pass the index to a float variable
        float freqN = maxN;
        
        // Interpolate index using neighbours
        if (maxN > 0 && maxN < SAMPLECOUNT - 1) {
            float dL = spectrumData [maxN - 1] / spectrumData [maxN];
            float dR = spectrumData [maxN + 1] / spectrumData [maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        
        // Convert index to frequency
        pitchValue = freqN * (samplerate / 2) / SAMPLECOUNT;
    }
    
    private void DeriveBlow() {
        
        UpdateRecords (dbValue, dbValues);
        UpdateRecords (pitchValue, pitchValues);
        
        // Find the average pitch in our records (used to decipher against whistles, clicks, etc).
        float sumPitch = 0;
        foreach (float num in pitchValues) {
            sumPitch += num;
        }
        sumPitch /= pitchValues.Count;
        
        // Run our low pass filter.
        lowPassResults = LowPassFilter (dbValue);
        
        // Decides whether this instance of the result could be a blow or not.
        if (lowPassResults > -30 && sumPitch == 0) {
            blowingTime += 1;
        } else {
            blowingTime = 0;
        }
        
        // Once enough successful blows have occured over the previous frames (requiredBlowTime), the blow is triggered.
        // This example says "blowing", or "not blowing", and also blows up a sphere.
        if (blowingTime > requiedBlowTime) {
        //    debugDisplay.guiText.text = "Blowing";
            //GameObject.FindGameObjectWithTag ("Meter").transform.localScale *= 1.012f;
        } else {
//            debugDisplay.guiText.text = "Not Blowing";
            //GameObject.FindGameObjectWithTag ("Meter").transform.localScale *= 0.999f;
        }
    }
    
    // Updates a record, by removing the oldest entry and adding the newest value (val).
    private void UpdateRecords(float val, List<float> record) {
        if (record.Count > recordedLength) {
            record.RemoveAt (0);
        }
        record.Add (val);
    }
    
    /// Gives a result (I don't really understand this yet) based on the peak volume of the record
    /// and the previous low pass results.
    private float LowPassFilter(float peakVolume) {
        return ALPHA * peakVolume + (1.0f - ALPHA) * lowPassResults;
    }
}
