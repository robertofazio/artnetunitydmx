﻿using UnityEngine;
using System;
using System.Collections;
using ArtNet;


public class lerpDmx : MonoBehaviour {

	private Color colorA = Color.red;
	private Color colorB = Color.yellow;
	
	ArtNet.Engine ArtEngine; 
	
	// Use this for initialization
	void Start () 
	{
		//istanzio il gestore della comunicazione:
		ArtEngine = new ArtNet.Engine("Open DMX Etheret", "255.255.255.255");
		//apro il canale di comunicazione con i parametri inseriti sopra:
		ArtEngine.Start ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.UpArrow))
			renderer.material.color = colorA;
		if(Input.GetKey(KeyCode.DownArrow))
			renderer.material.color = colorB;


		//creo un array di 512 byte per inserirci il messaggio:
		byte[] DMXData = new byte[512];

		//scrivo il messaggio che volgio mandare:
		DMXData [0] = 0;
		DMXData [1] = 0;
		DMXData [2] = 0;

		//mando il messaggio:
		ArtEngine.SendDMX (0, DMXData, DMXData.Length);

	
	}
}
