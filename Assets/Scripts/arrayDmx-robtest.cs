﻿using UnityEngine;
using System;
using System.Collections;
//using ArtNet;

public class arrayDmx : MonoBehaviour {

	//ArtNet.Engine 						ArtEngine; 
	public int 							numStripeLedRGB     	= 8;		// numbers of RGB stripe LED
	private float						gap						= 1.4f;
	public bool							sendDMX					= false;
	public GameObject[] 				cubes;
	public GameObject					shape;
	public Color32[]		    		cols;
	public float[]						loud_temp_g;
	public float[]						loud_temp_r;
	public float[]						loud_temp_b;
	public byte[]						ctb;
	public float						LOUD;
	public float[] 						spectrum				= new float[1024];

	public 

	void Awake ()
	{
		cubes 		= new GameObject[numStripeLedRGB];
		cols 		= new Color32[numStripeLedRGB];
		loud_temp_g = new float[numStripeLedRGB];
		loud_temp_r = new float[numStripeLedRGB];
		loud_temp_b = new float[numStripeLedRGB];
	}

	void Start () 
	{	
		for (int i = 0; i< numStripeLedRGB; i++) 
		{
			cubes[i] = Instantiate(shape , new Vector3(0, 0, i*gap) , transform.rotation ) as GameObject;
			//Camera.main.transform.LookAt (cubes[i].transform.position);
		}
		if(Input.GetKey(KeyCode.D))
		{
			initDmx();
			sendDMX = true;
		}
	}

	void Update () 
	{	
		// drawing on view port
		for (int i = 0; i< numStripeLedRGB; i++)
		{	
			// select 8 color32 in Gui to change each stripesLed Color
		    cubes[i].renderer.material.color = cols[i];
		}
		//creo un array di 512 byte per inserirci il messaggio:
		//byte[] DMXData = new byte[512];
		// convert Color32[] into byte[] 
		//ctb = Color32toByte.Color32ArrayToByteArray(cols);
	
		//float[] ff = LineInAudioSoundAnalisys.spectrum;
		// convert float[] to byte[]
		//byte[] FF = BitConverter.GetBytes(ff[0]);

		/*DMXData[0] =  FF[0];
		DMXData[1] =  FF[1];
		DMXData[2] =  FF[2];*/

		//cubes[0].renderer.material.color = new Color (FF[0], FF[1] ,FF[2]);

		//scrivo il messaggio numStripeLedRGBe volgio mandare:
		//DMXData  = ctb;

		if(sendDMX == true)
		{ 
			//mando il messaggio:
			//ArtEngine.SendDMX (0, DMXData, DMXData.Length);
		}
		LOUD = GetSpectrumData.loudness;
		Fa();

		for (int i = 0; i< numStripeLedRGB; i++)
		{	
			float loud_math_g = spectrum[256] * 1000 / (i + 1);
			float loud_math_r = spectrum[512] * 1000000 / (i + 1);
			float loud_math_b = spectrum[768] * 5000 / (i + 1);

			/*loud_temp_g[i] = loud_math_g;
			loud_temp_r[i] = loud_math_r;
			loud_temp_b[i] = loud_math_b;*/

			if(loud_math_g >= loud_temp_g[i])
				loud_temp_g[i] = loud_math_g;

			if(loud_math_r >= loud_temp_r[i])
				loud_temp_r[i] = loud_math_r;

			if(loud_math_b >= loud_temp_b[i])
				loud_temp_b[i] = loud_math_b;

			// select 8 color32 in Gui to change each stripesLed Color
			if(i < 6)
			{
				/*cols[i] = new Color(Mathf.PingPong(Time.time + (i + 1), loud_temp[i]) / 3, 
				                    loud_temp[i], 
				                    Mathf.PingPong(Time.time * (i + 1), loud_temp[i]) / 1.5f, 
				                    1);*/

				cols[i] = new Color(loud_temp_r[i], loud_temp_g[i], loud_temp_b[i], 1);
			}
			else
			{
				/*cols[i] = new Color(loud_temp[i], 
				                    Mathf.PingPong(Time.time + (i + 1), loud_temp[i]) / 3, 
				                    Mathf.PingPong(Time.time * (i + 1), loud_temp[i]) / 1.5f, 
				                    1);*/

				cols[i] = new Color(loud_temp_r[i], loud_temp_g[i], loud_temp_b[i], 1);
			}

			//Debug.Log((spectrum[256] * 100) + ", " + (spectrum[512] * 100) + ", " + (spectrum[768] * 100));

			int fade_amount = 3; 

			loud_temp_g[i] -= (loud_temp_g[i] / ((i + 1) * fade_amount));
			loud_temp_r[i] -= (loud_temp_r[i] / ((i + 1) * fade_amount));
			loud_temp_b[i] -= (loud_temp_b[i] / ((i + 1) * fade_amount));
		}
	}

	void Fa ( ) 
	{
		spectrum = GetSpectrumData.spectrumData;
		for (int i = 0; i< numStripeLedRGB*3; i++)
		{	
			//Color32[] FaCols = new Color32[numStripeLedRGB];
			byte[] spectrumBT = BitConverter.GetBytes( spectrum[i] );
			//Debug.Log(spectrumBT[i].ToString());
		    //float[] tmpC = cubes[i].renderer.material.color.r;
			//tmpC = spectrumBT[i];
		}

	}

	void initDmx ( )
	{
		//istanzio il gestore della comunicazione:
		//ArtEngine = new ArtNet.Engine("Open DMX Ethernet", "192.168.0.111");
		//apro il canale di comunicazione con i parametri inseriti sopra:
		//ArtEngine.Start ();
	}

	void OnApplicationQuit ( )
	{	
		for (int i = 0; i< numStripeLedRGB; i++) 
		{	//set all light to black on quit application
			cubes[ i ].renderer.material.color = new Color(0, 0, 0);
			byte[] setBlack = new byte[512];
			//ArtEngine.SendDMX (0, setBlack, setBlack.Length);
		}
	}
}


