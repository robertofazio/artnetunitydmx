﻿using UnityEngine;
using System;
using System.Collections;
using ArtNet;

public class arraydmx : MonoBehaviour 
{

	ArtNet.Engine 						ArtEngine; 
	public int 							numStripeLedRGB     	= 8;		// numbers of RGB stripe LED
	private float						gap						= 1.4f;
	public GameObject[] 				cubes;
	public GameObject					shape;
	public Color32[]		    		cols;
	public byte[]						ctb;
	public float						LOUD;
	public float[] 						spectrum;
	public byte[] 						DMXData;

	void Awake ()
	{
		cubes 		= new GameObject[numStripeLedRGB];
		cols 		= new Color32[numStripeLedRGB];
	}

	void Start () 
	{	
		for (int i = 0; i< numStripeLedRGB; i++) 
		{
			cubes[i] = Instantiate(shape , new Vector3(0, 0, i*gap) , transform.rotation ) as GameObject;
			//Camera.main.transform.LookAt (cubes[i].transform.position);
		}

			initDmx();

	}

	void Update () 
	{	
		// drawing on view port
		for (int i = 0; i< numStripeLedRGB; i++)
		{	
			if(i < 8)
				cols[i] = new Color(0, LOUD*30 / (i + 1), 0, 1);
			else
				cols[i] = new Color(LOUD / (i + 1), 0, 0, 1);
			// select 8 color32 in Gui to change each stripesLed Color
		     cubes [i].renderer.material.color = cols[i];
			// convert Color32[] into byte[] 
			ctb = Color32toByte.Color32ArrayToByteArray(cols);

			float[] ff = LineInAudioSoundAnalisys.spectrum;

			DMXData[i] = (byte)ReMap.Remap(spectrum[i]*100000, 0, 5, 0, 255);
			Debug.Log(DMXData[i]);
		}


	

		// convert float[] to byte[]
		//byte[] FF = BitConverter.GetBytes(ff[0]);

	




//		DMXData[1] =  FF[1];
//		DMXData[2] =  FF[2];



		//cubes[0].renderer.material.color = new Color (FF[0], FF[1] ,FF[2]);

		//scrivo il messaggio numStripeLedRGBe volgio mandare:
		//DMXData  = ctb;


		//mando il messaggio:
		ArtEngine.SendDMX (0, DMXData, DMXData.Length);

		LOUD = GetSpectrumData.loudness;
		Fa();

	}

	void Fa ( ) 
	{
		spectrum = GetSpectrumData.spectrumData;
		for (int i = 0; i< numStripeLedRGB*3; i++)
		{	
			Color32[] FaCols = new Color32[numStripeLedRGB];
			byte[] spectrumBT = BitConverter.GetBytes( spectrum[i] );
			Debug.Log(spectrumBT[i].ToString());
		    //float[] tmpC = cubes[i].renderer.material.color.r;
			//tmpC = spectrumBT[i];
		}

	}

	void initDmx ( )
	{

		//creo un array di 512 byte per inserirci il messaggio:
		DMXData = new byte[512];
		//istanzio il gestore della comunicazione:
		ArtEngine = new ArtNet.Engine("Open DMX Ethernet", "192.168.0.111");
		//apro il canale di comunicazione con i parametri inseriti sopra:
		ArtEngine.Start ();
	}

	void OnApplicationQuit ( )
	{	
		for (int i = 0; i< numStripeLedRGB; i++) 
		{	//set all light to black on quit application
			cubes[ i ].renderer.material.color = new Color(0, 0, 0);
			byte[] setBlack = new byte[512];
			ArtEngine.SendDMX (0, setBlack, setBlack.Length);
		}
	}
}


