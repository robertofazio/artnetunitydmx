﻿using UnityEngine;
using System.Collections;

public class simpleMeshDef : MonoBehaviour 
{

	public GameObject 		modifyObject;
	Mesh					myMesh;
	public Vector3[]		vertici;
	public Vector3[]		newVert;
	public Transform		refer;
	public float[]			audioSpectrum;

	public float 			auVal;
	public float			mod 				= 1.0f;
	public float 			power 				= 3.0f;
	public float 			scale 			    = 1.0f;
	public Vector2 			v2SampleStart 		= new Vector2(0f, 0f);


	void Awake ( )
	{
		auVal = 10.0f;
	}
	void Start ( ) 
	{	
		myMesh = modifyObject.GetComponent<MeshFilter>().mesh;
		vertici = new Vector3[ myMesh.vertexCount ];

		for(int i = 0; i < myMesh.vertexCount; i++)
		{
			myMesh.vertices[i] = new Vector3( myMesh.vertices[i].x, myMesh.vertices[i].y, myMesh.vertices[i].z );
		}

		vertici = myMesh.vertices;
	
	}
	
	// Update is called once per frame
	void Update ( ) 
	{	
		audioSpectrum = GetSpectrumData.spectrumData;

		newVert = myMesh.vertices;


			for(int i = 0; i < myMesh.vertexCount; i++)
			{
				// newVert[ i ] = vertici[ i ] * ( Mathf.Sin(Time.time) * mod * GetSpectrumData.loudness);
				float xCoord = v2SampleStart.x + newVert[i].x  * scale;
				float yCoord = v2SampleStart.y + newVert[i].z  * scale;
				
				newVert[i].y = vertici[i].y +( Mathf.PerlinNoise (xCoord, yCoord) - 0.5f) * power * GetSpectrumData.loudness; 
				
			}
			myMesh.vertices = newVert;
			
			myMesh.RecalculateNormals(); 
			myMesh.RecalculateBounds();

		

		if(Input.GetKey(KeyCode.N))
		{
			SpectrumMesh();
		}

	}

	void SpectrumMesh()
	{	  

		for(int i = 0; i < myMesh.vertexCount; i++)
		{	
			while (myMesh.vertexCount <= GetSpectrumData.SAMPLECOUNT)
			{
				newVert[i].y = audioSpectrum[i];
			}
			myMesh.vertices = newVert;
			
			myMesh.RecalculateNormals(); 
			myMesh.RecalculateBounds();


		}
	}
}
